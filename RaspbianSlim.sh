# GUI-related packages
pkgs="
gstreamer1.0-x gstreamer1.0-omx gstreamer1.0-plugins-base
gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-alsa
gstreamer1.0-libav
netsurf-gtk
weston
omxplayer
qt50-snapshot qt50-quick-particle-examples
"

# Jessie specific packages
pkgs="$pkgs
libreoffice
nodered
bluej
greenfoot"

# Edu-related packages
pkgs="$pkgs
python3-pygame python-pygame 
python-gpiozero python3-gpiozero
python-tk python3-tk
python-serial python3-serial
python-picamera python3-picamera
python3-pygame python-pygame python-tk
python3-tk
debian-reference-en dillo x2x
scratch nuscratch
timidity
smartsim penguinspuzzle
pistore
sonic-pi
python3-numpy
python3-pifacecommon python3-pifacedigitalio python3-pifacedigital-scratch-handler python-pifacecommon python-pifacedigitalio
oracle-java8-jdk
minecraft-pi python-minecraftpi
"

# Remove packages
for i in $pkgs; do
	echo apt-get -y remove --purge $i
done

# Remove automatically installed dependency packages
echo apt-get -y autoremove

# Remove games
echo rm -R /home/pi/python_games

# Remove all packages marked rc (thanks @symm)
dpkg --list |grep "^rc" | cut -d " " -f 3 | xargs dpkg --purge 